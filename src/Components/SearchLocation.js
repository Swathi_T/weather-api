import React from 'react'

function SearchLocation() {
  return (
    <form  className="form-container">
      <div className="input-container">
        <input
          className="form-control"
          type="search"
          placeholder ="Enter a city"
          value=''
          required
        />

        <div className="search-btn-container" >
          <button className="search-btn" type="button">
            Search
          </button>
        </div>
      </div>
  </form>
    
  )
}

export default SearchLocation