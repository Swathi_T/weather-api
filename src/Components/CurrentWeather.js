import React from 'react'

const CurrentWeather = () => {
  return (
    <div className="currentweather-conatiner">
      <h2 className="location-name">Location</h2>
      <h4 className="weather-short-description">status clouds/sunny</h4>
      <h1 className="currentweather-degree">20</h1>
      <div className="d-flex">
        <p className="high">High</p>
        <p className="low">Low</p>
      </div>
    </div>
  );
}

export default CurrentWeather