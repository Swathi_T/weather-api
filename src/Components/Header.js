import React from 'react'

function Header(tempScale) {
    const celsiusFontWeight = tempScale === 'celsius' ? 'bolder' : 'normal';
    const fahrenheitFontWeight = tempScale === 'fahrenheit' ? 'bolder' : 'normal';
    return ( 
      
        <nav className="navbar">
            <div className="heading-container">
              <h1 className='heading-header'>Weather Information</h1>
            </div>
            
            <div className="degree">
              <button  type="button" className="btn-degrees">
                <span style={{ fontWeight: celsiusFontWeight }} >°C</span> /{' '}
                <span style={{ fontWeight: fahrenheitFontWeight }} >°F</span>
              </button>
            </div>
        </nav>
        
      );
}
export default Header
