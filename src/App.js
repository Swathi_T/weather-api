import './style.css';
import Header from './Components/Header';
import SearchLocation from './Components/SearchLocation'
import CurrentWeather from './Components/CurrentWeather';

function App() {
  return (
    <div className="App">
      <Header/>
      <SearchLocation/>
      <CurrentWeather/>
    </div>
  );
}

export default App;
